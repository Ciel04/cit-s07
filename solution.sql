Adding records to users Table:
	INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("janesmithh@gmail.com", "passwordC", "2021-01-01 03:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");
Adding records to posts Table:
	INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");
	INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
	INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
Get all the post with an author ID of 1:
	Select * FROM posts WHERE author_id=1;
Get all the users email and datetime of creation:
	SELECT email, datetime_created FROM users;
Update a posts content to Hello to the people of the Earth! where its initial content is Hello Earth! by using the records ID:
	UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id=2;
Delete the user with an email of johndoe@gmail.com:
	DELETE FROM users WHERE email="johndoe@gmail.com";
